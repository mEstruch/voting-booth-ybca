var socket = io();

var bannerCounter = 0;

socket.on('initialize', function(data){

  console.log(data.images);
  // allImages = data.images;
  for(var i in data.images){
    allImages.push({"src":data.images[i], "banner":bannerCounter});
    bannerCounter++;
    if(bannerCounter > 2) bannerCounter = 0;
  }
  updateWithImages();
});

socket.on('new-image', function(data){

  console.log("New image!"+data.image);
  allImages.unshift({"src":data.image, "banner":bannerCounter});
  bannerCounter++;
  if(bannerCounter > 2) bannerCounter = 0;
  // allImages.unshift([data.image]);
  updateWithImages();
});
