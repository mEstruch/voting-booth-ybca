var allImages = [];


function updateWithImages(){
  var top = $(".top-container .container-inner");
  var middle = $(".middle-container .container-inner");
  var bottom = $(".bottom-container .container-inner");
  top.html("");middle.html("");bottom.html("");
  var totalHtml = ["","",""];

  allImages.slice().forEach(function(image) {
    var randomBanner = Math.floor(Math.random()*3);
    var imageSrc = '<div class="image-container"><img src="media/'+image.src+'" data-toggle-fullscreen/></div>';
    totalHtml[image.banner]+=imageSrc;
  });
  top.html(totalHtml[0]);
  middle.html(totalHtml[1]);
  bottom.html(totalHtml[2]);
}

document.addEventListener('click', function (event) {

  // Ignore clicks that weren't on the toggle button
  if (!event.target.hasAttribute('data-toggle-fullscreen')) return;

  // If there's an element in fullscreen, exit
  // Otherwise, enter it
  if (document.fullscreenElement) {
    console.log("Exit full screen");
    document.exitFullscreen();
  } else {
    console.log("Go full screen");
    document.documentElement.requestFullscreen();
  }

}, false);


window.onkeyup = function(e) {
 var key = e.keyCode ? e.keyCode : e.which;
 if (key == 70) {
     if (document.fullscreenElement) {
       console.log("Exit full screen");
      document.exitFullscreen();
    } else {
       console.log("Go full screen");
      document.documentElement.requestFullscreen();
    }
 }
}
