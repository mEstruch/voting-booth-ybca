const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const MessagingResponse = require('twilio').twiml.MessagingResponse;

const mediaFolder = './public/media/';

const PORT = process.env.OPENSHIFT_NODEJS_PORT || 5000;
var IP_ADDRESS = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';


const app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);


app.use(bodyParser.urlencoded({ extended: false } ));

app.post('/sms', (req, res) => {
  const twiml = new MessagingResponse();

  if(req.body.NumMedia !== '0') {
    const imageName = formatDate(new Date())+"_"+req.body.MessageSid+".jpg";
    const filename = "public/media/"+imageName;
    const url = req.body.MediaUrl0;

    // Download the image.
    request(url).pipe(fs.createWriteStream(filename))
      .on('close', () =>{
          io.emit('new-image', { image: imageName });
          console.log('Image downloaded.')
        });

    twiml.message('Thanks for the image! Your image will appear soon on the screen.');
  } else {
    twiml.message("Try sending a picture message.");
  }

  res.send(twiml.toString());
});

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.use(express.static('public'))

io.on('connection', function(socket){
  console.log('a user connected ');
  var initializedData = [];
  fs.readdirSync(mediaFolder).forEach(file => {

    if(file.charAt(0) != '.'){
      initializedData.unshift(file);
    }

  });

  socket.emit('initialize', { images: initializedData });
});
http.listen(PORT, function(){
  console.log('webiste listening on localhost:'+PORT);
});


function formatDate(date) {

  return date.getFullYear()+"_"+date.getMonth()+"_"+(date.getDay()+1)+"_"+date.getHours()+"_"+date.getMinutes()+"_"+date.getSeconds()+"_"+date.getMilliseconds();
}

// app.listen(PORT, () => console.log('Twilio listening at localhost:1337!'));
